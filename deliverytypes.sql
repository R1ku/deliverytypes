-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 23 2020 г., 15:55
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `deliverytypes`
--

-- --------------------------------------------------------

--
-- Структура таблицы `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `available` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `countries`
--

INSERT INTO `countries` (`id`, `name`, `available`) VALUES
(1, 'Afghanistan', 0),
(2, 'Albania', 0),
(3, 'Algeria', 0),
(4, 'Angola', 0),
(5, 'Anguilla', 0),
(6, 'Antigua and Barbuda', 0),
(7, 'Argentina', 0),
(8, 'Armenia', 0),
(9, 'Ascension', 0),
(10, 'Australia', 1),
(11, 'Austria', 0),
(12, 'Azerbaijan', 0),
(13, 'Bahamas', 0),
(14, 'Bahrain', 0),
(15, 'Bangladesh', 0),
(16, 'Barbados', 0),
(17, 'Belarus', 0),
(18, 'Belgium', 0),
(19, 'Belize', 0),
(20, 'Benin', 0),
(21, 'Bermuda', 0),
(22, 'Bhutan', 0),
(23, 'Bolivia', 0),
(24, 'Bosnia and Herzegovina', 0),
(25, 'Botswana', 0),
(26, 'Brazil', 0),
(27, 'British Virgin Islands', 0),
(28, 'Brunei', 0),
(29, 'Bulgaria', 0),
(30, 'Burkina Faso', 0),
(31, 'Burundi', 0),
(32, 'Cambodia', 0),
(33, 'Cameroon', 0),
(34, 'Canada', 0),
(35, 'Cabo Verde', 0),
(36, 'Cayman', 0),
(37, 'Central African', 0),
(38, 'Chile', 0),
(39, 'China', 0),
(40, 'Christmas Island', 0),
(41, 'Cocos (Keeling) Islands', 0),
(42, 'Colombia', 0),
(43, 'Comores', 0),
(44, 'Congo', 0),
(45, 'Cook Islands', 0),
(46, 'Costa Rica', 0),
(47, 'Côte d\'Ivoire', 0),
(48, 'Croatia', 0),
(49, 'Cuba', 0),
(50, 'Cyprus', 1),
(51, 'Czech', 0),
(52, 'Denmark', 0),
(53, 'Djibouti', 0),
(54, 'Dominica', 0),
(55, 'East Timor', 0),
(56, 'Ecuador', 0),
(57, 'Egypt', 0),
(58, 'El Salvador', 0),
(59, 'Eritrea', 0),
(60, 'Estonia', 0),
(61, 'Ethiopia', 0),
(62, 'Falkland Islands', 0),
(63, 'Faroe Islands', 0),
(64, 'Fiji', 0),
(65, 'Finland', 0),
(66, 'France', 0),
(67, 'Macedonia', 0),
(68, 'Gabon', 0),
(69, 'Gambia', 0),
(70, 'Georgia', 0),
(71, 'Germany', 0),
(72, 'Ghana', 0),
(73, 'Gibraltar', 0),
(74, 'Greece', 0),
(75, 'Greenland', 0),
(76, 'Grenada', 0),
(77, 'Guadeloupe', 0),
(78, 'Guam', 0),
(79, 'Guatemala', 0),
(80, 'Guernsey', 0),
(81, 'Guiana', 0),
(82, 'Guinea', 0),
(83, 'Guinea Ecuatorial', 0),
(84, 'Guinea-Bissau', 0),
(85, 'Guyana', 0),
(86, 'Haiti', 0),
(87, 'Honduras', 0),
(88, 'Hong Kong', 0),
(89, 'Hungary', 0),
(90, 'Iceland', 0),
(91, 'India', 0),
(92, 'Indonesia', 0),
(93, 'Iran', 0),
(94, 'Iraq', 0),
(95, 'Ireland', 0),
(96, 'Isle of Man', 0),
(97, 'Israel', 0),
(98, 'Italy', 0),
(99, 'Jamaica', 0),
(100, 'Jersey', 0),
(101, 'Jordan', 0),
(102, 'Kazakhstan', 0),
(103, 'Kenya', 0),
(104, 'Kiribati', 0),
(105, 'Korea', 0),
(106, 'Kosovo', 0),
(107, 'Kuwait', 0),
(108, 'Kyrgyz', 0),
(109, 'Laos', 0),
(110, 'Latvia', 0),
(111, 'Lebanon', 0),
(112, 'Lesotho', 0),
(113, 'Liberia', 0),
(114, 'Libya', 0),
(115, 'Liechtenstein', 0),
(116, 'Lithuania', 0),
(117, 'Luxembourg', 0),
(118, 'Macao', 0),
(119, 'Madagascar', 0),
(120, 'Malawi', 0),
(121, 'Malaysia', 0),
(122, 'Maldives', 0),
(123, 'Mali', 0),
(124, 'Malta', 0),
(125, 'Marshall Islands', 0),
(126, 'Martinique', 0),
(127, 'Mauritania', 0),
(128, 'Mauritius', 0),
(129, 'Mexico', 0),
(130, 'Micronesia', 0),
(131, 'Moldova', 0),
(132, 'Monaco', 0),
(133, 'Mongolia', 0),
(134, 'Montenegro', 0),
(135, 'Montserrat', 0),
(136, 'Morocco', 0),
(137, 'Mozambique', 0),
(138, 'Myanmar', 0),
(139, 'Namibia', 0),
(140, 'Naoero', 0),
(141, 'Nepal', 0),
(142, 'Netherlands', 0),
(143, 'Netherlands Antilles', 0),
(144, 'New Caledonia', 0),
(145, 'New Zealand', 0),
(146, 'Nicaragua', 0),
(147, 'Niger', 0),
(148, 'Nigeria', 0),
(149, 'Norfolk Island', 0),
(152, 'Norway', 0),
(153, 'Oman', 0),
(154, 'Pakistan', 0),
(155, 'Panama', 0),
(156, 'Papua New Guinea', 0),
(157, 'Paraguay', 0),
(158, 'Peru', 0),
(159, 'Philippines', 0),
(160, 'Pitcairn', 0),
(161, 'Poland', 1),
(162, 'Polynesia', 0),
(163, 'Portugal', 0),
(164, 'Puerto Rico', 0),
(165, 'Qatar', 0),
(166, 'Reunion', 0),
(167, 'Romania', 0),
(168, 'Russia', 1),
(169, 'Rwanda', 0),
(170, 'Saint Christopher and Nevis', 0),
(171, 'Saint Helena', 0),
(172, 'Saint Lucia', 0),
(173, 'Saint Vincent ', 0),
(174, 'Saint Pierre and Miquelon', 0),
(175, 'Northern Mariana Islands', 0),
(176, 'Samoa', 0),
(177, 'San Marino', 0),
(178, 'Sao Tome and Principe', 0),
(179, 'Saudi Arabia', 0),
(180, 'Senegal', 0),
(181, 'Serbia', 0),
(182, 'Seychelles', 0),
(183, 'Sierra Leone', 0),
(184, 'Singapore', 0),
(185, 'Slovakia', 0),
(186, 'Slovenia', 0),
(187, 'Solomon', 0),
(188, 'Somalia', 0),
(189, 'South Africa', 0),
(190, 'South Sudan', 0),
(191, 'Spain', 0),
(192, 'Sri Lanka', 0),
(193, 'Sudan', 0),
(194, 'Suriname', 0),
(195, 'Swaziland', 0),
(196, 'Sweden', 1),
(197, 'Switzerland', 0),
(198, 'Syrian ', 0),
(199, 'French Southern Territories', 0),
(200, 'Taiwan', 0),
(201, 'Tajikistan', 0),
(202, 'Tanzania', 0),
(203, 'Tchad', 0),
(204, 'Thailand', 0),
(205, 'Togo', 0),
(206, 'Tonga', 0),
(207, 'Trinidad and Tobago', 0),
(208, 'Tristan da Cunha', 0),
(209, 'Tunisia', 0),
(210, 'Turkey', 0),
(211, 'Turkmenistan', 0),
(212, 'Turks and Caicos Island', 0),
(213, 'Tuvalu', 0),
(214, 'Uganda', 0),
(215, 'Great Britain', 0),
(216, 'Ukraine', 1),
(217, 'United Arab Emirates', 0),
(218, 'Virgin Islands (U.S.)', 0),
(219, 'Uruguay', 0),
(220, 'USA', 1),
(221, 'Uzbekistan', 0),
(222, 'Vanuatu', 0),
(223, 'Vatican', 0),
(224, 'Venezuela', 0),
(225, 'Vietnam', 0),
(227, 'Wallis and Futuna', 0),
(228, 'Yemen', 0),
(229, 'Zambia', 0),
(230, 'Zimbabwe', 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
