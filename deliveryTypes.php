<?php
require 'Carbon/autoload.php';

use Carbon\Carbon;
use Carbon\CarbonPeriod;

class deliveryAddressForm {
    public $type;

    public function __construct($type){
        $this->type = $type;
    }

    public function getForm() {
        $type = $this->type;
        if (method_exists($this, $type)) return $this->$type();
        else echo 'Извините, такого способа отправки нет';
    }

    public function Moscow() {
        $period = CarbonPeriod::create(Carbon::tomorrow(), Carbon::tomorrow()->addDays(10));
        include 'moscow.php';
    }

    public function Japan() {
        include 'japan.php';
    }

    public function World() {
        $db = OpenCon();
        $countries = $db->query('SELECT * FROM `countries` WHERE `available`=1 ORDER BY `name` DESC');
        CloseCon($db);
        include 'world.php';
    }
}

function OpenCon()
{
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $db = "deliverytypes";
    $conn = mysqli_connect($dbhost, $dbuser, $dbpass,$db) or die("Connect failed: %s\n". $conn -> error);

    return $conn;
}

function CloseCon($conn)
{
    $conn -> close();
}
